# JWT implementation with Spring Boot

### Using tools & technologies


* JDK 11
* Spring Boot 2.5.1
* MySQL 5.7
* Liquibase 
* Docker
* Gradle

 

### In root directory :
```dockerfile
docker-comopose -f docker-compose.yml up -d
```
### Run application
```bash
 ./gradlew run
```
### Authenticate and get a token :
```
POST - localhost:8081/api/v1/login
```
### Use this credentials:
```json
login: admin
password: admin
```
### Use token for authorization
```
GET - localhost:8081/api/v1/hello
```