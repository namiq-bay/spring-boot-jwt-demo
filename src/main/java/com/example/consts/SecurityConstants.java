package com.example.consts;

public class SecurityConstants {
    public static final String SECRET = "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdA==";
    public static final long TOKEN_EXPIRATION_TIME = 900_000; // 15 mins
    public static final long TOKEN_EXPIRATION_TIME_WITH_REMEMBER_ME = 4*900_000; // 1h
}
