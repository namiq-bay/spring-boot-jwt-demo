package com.example.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/")
public class MainController {

    @GetMapping("/hello")
    public String hello() {
        return "Hello World!";
    }

}
